#define DEBUG 1

int LEDpin =    0x00d;
int dotDelay =  0x480;
int CPM =       0x00f;


/* Define dots and dashes
   Dots and dashes for each character stored in a byte
   For alphanumeric characters, 3 MSB bits define number of symbols (1 to 5)
   For punctuation characters (',' and '.'), 2 MSB bits indicate 6 symbols
   Dot = 0, Dash = 1; LSB bits stored in reverse order so bit 0 = first symbol
   For example, F = "..--" = 4 symbols = 0010, so stored as 0100
   000 xxxxx 0 symbols (not used)
   001 xxxx? 1 symbol  (x = don't care = 0, ? = symbol data = 0 or 1)
   010 xxx?? 2 symbols
   011 xx??? 3 symbols
   100 x???? 4 symbols
   101 ????? 5 symbols
   11 ?????? 6 symbols
*/
byte dotsNdashes[] = {B01000010,  //  0 = A ".-"     ('A' = ASCII code 65; 'a' = ASCII code 97)
                      B10000001,  //  1 = B "-..."
                      B10000101,  //  2 = C "-.-."
                      B01100001,  //  3 = D "-.."
                      B00100000,  //  4 = E "."
                      B10000100,  //  5 = F "..-."
                      B01100011,  //  6 = G "--."
                      B10000000,  //  7 = H "...."
                      B01000000,  //  8 = I ".."
                      B10001110,  //  9 = J ".---"
                      B01100101,  // 10 = K "-.-"
                      B10000010,  // 11 = L ".-.."
                      B01000011,  // 12 = M "--"
                      B01000001,  // 13 = N "-."
                      B01100111,  // 14 = O "---"
                      B10000110,  // 15 = P ".--."
                      B10001011,  // 16 = Q "--.-"
                      B01100010,  // 17 = R ".-."
                      B01100000,  // 18 = S "..."
                      B00100001,  // 19 = T "-"
                      B01100100,  // 20 = U "..-"
                      B10001000,  // 21 = V "...-"
                      B01100110,  // 22 = W ".--"
                      B10001001,  // 23 = X "-..-"
                      B10001101,  // 24 = Y "-.--"
                      B10000011,  // 25 = Z "--.."
                      B10111111,  // 26 = 0 "-----"  (ASCII code 48)
                      B10111110,  // 27 = 1 ".----"
                      B10111100,  // 28 = 2 "..---"
                      B10111000,  // 29 = 3 "...--"
                      B10110000,  // 30 = 4 "....-"
                      B10100000,  // 31 = 5 "....."
                      B10100001,  // 32 = 6 "-...."
                      B10100011,  // 33 = 7 "--..."
                      B10100111,  // 33 = 8 "---.."
                      B10101111,  // 34 = 9 "----."
                      B11110011,  // 36 = , "--..--" (ASCII code 44)
                      B11101010,  // 37 = . ".-.-.-" (ASCII code 46)
                      B11001100   // 38 = ? "..--.." (ASCII code 63)
                     };


void setup() {
  // initialize the digital pin as an output.
  pinMode(LEDpin, OUTPUT);
  digitalWrite(LEDpin, LOW);
  Serial.begin(9600);
}


void loop() {
  char value[] = "SOS ";
  convertInput(value);
}


void convertInput(const char inputVal[]) {
  for (int i = 0; inputVal[i] != '\0'; i++) {
    if (inputVal[i] >= 'a' && inputVal[i] <= 'z')
      blink(dotsNdashes[inputVal[i] - 'a']);
    else if (inputVal[i] >= 'A' && inputVal[i] <= 'Z')
      blink(dotsNdashes[inputVal[i] - 'A']);
    else if (inputVal[i] >= '0' && inputVal[i] <= '9')
      blink(dotsNdashes[inputVal[i] - '0' + 0x1A]);
    else if (inputVal[i] == ',')
      blink(dotsNdashes[36]);
    else if (inputVal[i] == '.')
      blink(dotsNdashes[37]);
    else if (inputVal[i] == '?')
      blink(dotsNdashes[38]);
  }
}


void blink(byte b_bNd) {
  int i_bndLen;

  // Mask to get morse code length from byte
  i_bndLen = (b_bNd >> 0x05);

  // Check for punctuation
  if ((i_bndLen >= 0x06) && !(dotsNdashes[38]) )
    i_bndLen &= B00000110;

  // Blink LED
  for (int i = 0; i < i_bndLen; i++) {
    if ((b_bNd & B01) == 0) {
      digitalWrite(LEDpin, HIGH);
      // Hold 'dot' for one unit
      delay(dotDelay / CPM);
      digitalWrite(LEDpin, LOW);
    }
    else if ((b_bNd & B01) == 1) {
      digitalWrite(LEDpin, HIGH);
      // Hold 'dash' for three units
      delay(dotDelay * 0x03 / CPM);
      digitalWrite(LEDpin, LOW);
    }
    else {
      // Handle space
      digitalWrite(LEDpin, LOW);
      // The space between words is seven units
      delay(dotDelay * 0x07 / CPM);
    }
    // The space between letters is 3 units
    delay(dotDelay * 0x03 / CPM);

    // Shift binary value to the right by one to get next bit
    b_bNd = b_bNd >> 0x01;
  }

  // Adds space at the end of a string value
  delay(dotDelay * 0x07 / CPM);
}

